package test.task.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Triangle {

    @Id
    private Long id;

    private Long a;

    private Long b;

    private Long c;
}
