package test.task.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Rectangle {

    @Id
    private Long id;

    private Long top;

    private Long right;

    private Long button;

    private Long left;
}
