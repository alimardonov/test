package test.task.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import test.task.dto.ErrorDto;
import test.task.exception.NotFoundObjectException;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity generalErrorHandler(RuntimeException e) {
        return ResponseEntity.badRequest()
                .body(ErrorDto.fromException(e));
    }

    @ExceptionHandler({NotFoundObjectException.class})
    public ResponseEntity categoryNotFoundException(NotFoundObjectException e) {
        return ResponseEntity.badRequest()
                .body(ErrorDto.notFoundException(2, "ERROR", e.getLocalizedMessage()));
    }
}
