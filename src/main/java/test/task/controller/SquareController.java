package test.task.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import test.task.service.SquareService;
import test.task.service.TriangleService;

@RestController
@RequestMapping("api/v1/square")
public class SquareController {

    private final SquareService service;

    public SquareController(SquareService service) {
        this.service = service;
    }

    @GetMapping("/{id}/surface")
    public ResponseEntity surface(@PathVariable Long id){
        return ResponseEntity.ok(service.surface(id));
    }

    @GetMapping("/{id}/length")
    public ResponseEntity length(@PathVariable Long id){
        return ResponseEntity.ok(service.length(id));
    }
}
