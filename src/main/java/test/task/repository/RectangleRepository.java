package test.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import test.task.entity.Rectangle;

import java.util.List;
import java.util.Optional;

@Repository
public interface RectangleRepository extends JpaRepository<Rectangle, Long> {

    Optional<Rectangle> findById(Long id);

    List<Rectangle> findAll();
}
