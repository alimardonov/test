package test.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import test.task.entity.Triangle;

import java.util.List;
import java.util.Optional;

@Repository
public interface TriangleRepository extends JpaRepository<Triangle, Long> {

    Optional<Triangle> findById(Long id);

    List<Triangle> findAll();
}
