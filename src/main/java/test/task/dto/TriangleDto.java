package test.task.dto;

import lombok.Getter;
import lombok.Setter;
import test.task.entity.Triangle;

@Getter
@Setter
public class TriangleDto {

    private Long a;

    private Long b;

    private Long c;

    public static TriangleDto fromTriangle(Triangle triangle) {
        TriangleDto dto = new TriangleDto();
        dto.setA(triangle.getA());
        dto.setB(triangle.getB());
        dto.setC(triangle.getC());
        return dto;
    }
}
