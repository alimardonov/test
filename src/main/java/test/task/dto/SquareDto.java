package test.task.dto;

import lombok.Getter;
import lombok.Setter;
import test.task.entity.Square;

@Getter
@Setter
public class SquareDto {
    private Long a;

    public static SquareDto fromSquare(Square square) {
        SquareDto dto = new SquareDto();
        dto.setA(square.getA());
        return dto;
    }
}
