package test.task.dto;

import lombok.Getter;
import lombok.Setter;
import test.task.entity.Rectangle;

@Getter
@Setter
public class RectangleDto {

    private Long top;

    private Long right;

    private Long button;

    private Long left;

    public static RectangleDto fromRectangle(Rectangle rectangle) {
        RectangleDto dto = new RectangleDto();
        dto.setTop(rectangle.getTop());
        dto.setRight(rectangle.getRight());
        dto.setButton(rectangle.getButton());
        dto.setLeft(rectangle.getLeft());
        return dto;
    }
}
