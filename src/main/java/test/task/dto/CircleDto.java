package test.task.dto;

import lombok.Getter;
import lombok.Setter;
import test.task.entity.Circle;

@Getter
@Setter
public class CircleDto {

    private Long r;

    public static CircleDto formCircle(Circle dto){
        CircleDto circleDto = new CircleDto();
        circleDto.setR(dto.getR());
        return circleDto;
    }
}
