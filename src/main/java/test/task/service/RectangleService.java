package test.task.service;

import test.task.dto.RectangleDto;
import test.task.entity.Rectangle;

import java.util.List;

public interface RectangleService {

    Long surface(Long id);

    Long length(Long id);

    RectangleDto add(RectangleDto dto);

    List<Rectangle> list();
}
