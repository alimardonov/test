package test.task.service;

import test.task.dto.SquareDto;
import test.task.entity.Square;

import java.util.List;

public interface SquareService {

    Long surface(Long a);

    Long length(Long a);

    SquareDto add(SquareDto dto);

    List<Square> list();
}
