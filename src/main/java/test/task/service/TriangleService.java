package test.task.service;

import test.task.dto.TriangleDto;
import test.task.entity.Triangle;

import java.util.List;

public interface TriangleService {

    Double surface(Long id);

    Long length(Long id);

    TriangleDto add(TriangleDto dto);

    List<Triangle> list();
}
