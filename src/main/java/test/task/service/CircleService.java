package test.task.service;

import test.task.dto.CircleDto;
import test.task.entity.Circle;

import java.util.List;

public interface CircleService {

    Double surface(Long id);

    Double length(Long id);

    CircleDto add(CircleDto dto);

    List<Circle> list();
}

