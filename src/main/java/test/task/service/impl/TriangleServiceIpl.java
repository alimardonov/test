package test.task.service.impl;

import org.springframework.stereotype.Service;
import test.task.dto.TriangleDto;
import test.task.entity.Triangle;
import test.task.exception.NotFoundObjectException;
import test.task.repository.TriangleRepository;
import test.task.service.TriangleService;

import java.util.List;
import java.util.Optional;

@Service
public class TriangleServiceIpl implements TriangleService {

    private final TriangleRepository repository;

    public TriangleServiceIpl(TriangleRepository repository) {
        this.repository = repository;
    }

    @Override
    public Double surface(Long id) {
        Optional<Triangle> triangle = repository.findById(id);
        if (triangle.isPresent()) {
            Long p = triangle.get().getA() + triangle.get().getB() + triangle.get().getC();
            return Math.sqrt(p * (p - triangle.get().getA()) *
                    (p * triangle.get().getB()) * (p - triangle.get().getC()));
        } else throw new NotFoundObjectException(id);
    }

    @Override
    public Long length(Long id) {
        Optional<Triangle> triangle = repository.findById(id);
        if (triangle.isPresent()) {
            return triangle.get().getA() + triangle.get().getB() + triangle.get().getC();
        } else throw new NotFoundObjectException(id);
    }

    @Override
    public TriangleDto add(TriangleDto dto) {
        Triangle triangle = new Triangle();
        triangle.setA(dto.getA());
        triangle.setB(dto.getB());
        triangle.setC(dto.getC());
        return TriangleDto.fromTriangle(repository.save(triangle));
    }

    @Override
    public List<Triangle> list() {
        return repository.findAll();
    }
}
