package test.task.service.impl;

import org.springframework.stereotype.Service;
import test.task.dto.RectangleDto;
import test.task.entity.Rectangle;
import test.task.exception.NotFoundObjectException;
import test.task.repository.RectangleRepository;
import test.task.service.RectangleService;

import java.util.List;
import java.util.Optional;

@Service
public class RectangleServiceImpl implements RectangleService {

    private final RectangleRepository repository;

    public RectangleServiceImpl(RectangleRepository repository) {
        this.repository = repository;
    }

    @Override
    public Long surface(Long id) {
        Optional<Rectangle> rectangle = repository.findById(id);
        if (rectangle.isPresent()) {
            return rectangle.get().getTop() * rectangle.get().getRight() *
                    rectangle.get().getButton() * rectangle.get().getLeft();
        } else throw new NotFoundObjectException(id);
    }

    @Override
    public Long length(Long id) {
        Optional<Rectangle> rectangle = repository.findById(id);
        if (rectangle.isPresent()) {
            return rectangle.get().getTop() + rectangle.get().getRight() +
                    rectangle.get().getButton() + rectangle.get().getLeft();
        } else throw new NotFoundObjectException(id);
    }

    @Override
    public RectangleDto add(RectangleDto dto) {
        Rectangle rectangle = new Rectangle();
        rectangle.setTop(dto.getTop());
        rectangle.setRight(dto.getRight());
        rectangle.setButton(dto.getButton());
        rectangle.setLeft(dto.getLeft());
        return RectangleDto.fromRectangle(repository.save(rectangle));
    }

    @Override
    public List<Rectangle> list() {
        return repository.findAll();
    }
}
