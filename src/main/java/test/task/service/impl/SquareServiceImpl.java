package test.task.service.impl;

import org.springframework.stereotype.Service;
import test.task.dto.SquareDto;
import test.task.entity.Square;
import test.task.exception.NotFoundObjectException;
import test.task.repository.SquareRepository;
import test.task.service.SquareService;

import java.util.List;
import java.util.Optional;

@Service
public class SquareServiceImpl implements SquareService {

    private final SquareRepository repository;

    public SquareServiceImpl(SquareRepository repository) {
        this.repository = repository;
    }

    @Override
    public Long surface(Long id) {
        Optional<Square> square = repository.findById(id);
        if (square.isPresent()){
            return (square.get().getA() * square.get().getA() *
                    square.get().getA() * square.get().getA());
        } else throw new NotFoundObjectException(id);
    }

    @Override
    public Long length(Long id) {
        Optional<Square> square = repository.findById(id);
        if (square.isPresent()){
            return 4 * square.get().getA();
        } else throw new NotFoundObjectException(id);
    }

    @Override
    public SquareDto add(SquareDto dto) {
        Square square = new Square();
        square.setA(dto.getA());
        return SquareDto.fromSquare(repository.save(square));
    }

    @Override
    public List<Square> list() {
        return repository.findAll();
    }
}
