package test.task.service.impl;

import org.springframework.stereotype.Service;
import test.task.dto.CircleDto;
import test.task.entity.Circle;
import test.task.exception.NotFoundObjectException;
import test.task.repository.CircleRepository;
import test.task.service.CircleService;

import java.util.List;
import java.util.Optional;

@Service
public class CircleServiceImpl implements CircleService {

    private final CircleRepository repository;

    public CircleServiceImpl(CircleRepository repository) {
        this.repository = repository;
    }

    @Override
    public Double surface(Long id) {
        Optional<Circle> circle = repository.findById(id);
        if (circle.isPresent()) {
            return Math.PI * circle.get().getR() * circle.get().getR();
        } else throw new NotFoundObjectException(id);
    }

    @Override
    public Double length(Long id) {
        Optional<Circle> circle = repository.findById(id);
        if (circle.isPresent()) {
            return 2 * Math.PI * circle.get().getR();
        } else throw new NotFoundObjectException(id);
    }

    @Override
    public CircleDto add(CircleDto dto) {
        Circle circle = new Circle();
        circle.setR(dto.getR());
        return CircleDto.formCircle(repository.save(circle));
    }

    @Override
    public List<Circle> list() {
        return repository.findAll();
    }
}
