package test.task.exception;

public class NotFoundObjectException extends RuntimeException {
    public NotFoundObjectException(Long id) {
        super(String.format("object not found with ID: %d", id));
    }
}
